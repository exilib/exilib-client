package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.paulbenn.exilib.client.util.Enums;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public enum FrameType {

    @JsonProperty("0")
    NORMAL(0),
    @JsonProperty("1")
    MAGIC(1),
    @JsonProperty("2")
    RARE(2),
    @JsonProperty("3")
    UNIQUE(3),
    @JsonProperty("4")
    GEM(4),
    @JsonProperty("5")
    CURRENCY(5),
    @JsonProperty("6")
    DIVINATION_CARD(6),
    @JsonProperty("7")
    QUEST_ITEM(7),
    @JsonProperty("8")
    PROPHECY(8),
    @JsonProperty("9")
    RELIC(9);

    private static final Map<Integer, FrameType> lookUp = Enums.keyMap(FrameType.class, FrameType::getValue);

    private final int value;

    public static FrameType from(int frameType) {
        return lookUp.get(frameType);
    }
}
