package uk.co.paulbenn.exilib.client.model;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import uk.co.paulbenn.exilib.client.model.api.Socket;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@EqualsAndHashCode
public class Sockets {

    private static final int MAX_SOCKETS = 6;

    private final Map<Socket.Colour, Integer> byColour;
    private final Map<Integer, Integer> byGroup;

    private Sockets(Socket[] sockets) {
        byColour = Arrays.stream(sockets).collect(Collectors.toMap(Socket::getColour, s -> 1, Integer::sum));
        byGroup = Arrays.stream(sockets).collect(Collectors.toMap(Socket::getGroup, s -> 1, Integer::sum));
    }

    public static Sockets ofSockets(Socket... sockets) {
        return new Sockets(sockets);
    }

    public int getTotalSocketCount() {
        return byColour.values().size();
    }

    public int getWhiteSocketCount() {
        return byColour.getOrDefault(Socket.Colour.WHITE, 0);
    }

    public int getRedSocketCount() {
        return byColour.getOrDefault(Socket.Colour.RED, 0);
    }

    public int getGreenSocketCount() {
        return byColour.getOrDefault(Socket.Colour.GREEN, 0);
    }

    public int getBlueSocketCount() {
        return byColour.getOrDefault(Socket.Colour.BLUE, 0);
    }

    public int getAbyssSocketCount() {
        return byColour.getOrDefault(Socket.Colour.ABYSS, 0);
    }

    public int getDelveSocketCount() {
        return byColour.getOrDefault(Socket.Colour.DELVE, 0);
    }

    public int getLinkedGroupsCount() {
        return byGroup.keySet().size();
    }

    public int getLargestLinkedGroupSize() {
        return Collections.max(byGroup.values());
    }
}
