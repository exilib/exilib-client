package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public enum ValueType {

    // 0xFFFFFF = White
    @JsonProperty("0")
    DEFAULT(new int[] { 0 }, 0xFFFFFF),
    // 0x8888FF = Light Slate Blue
    @JsonProperty("1")
    AUGMENTED(new int[] { 1 }, 0x8888FF),
    // 0xD20000 = Free Speech Red
    @JsonProperty("2")
    UNMET(new int[] { 2 }, 0xD20000),
    // 0xFFFFFF = White
    @JsonProperty("3")
    PHYSICAL_DAMAGE(new int[] { 3 }, 0xFFFFFF),
    // 0x960000 = Dark Red
    @JsonProperty("4")
    FIRE_DAMAGE(new int[] { 4 }, 0x960000),
    // 0x366492 = Lochmara
    @JsonProperty("5")
    COLD_DAMAGE(new int[] { 5 }, 0x366492),
    // 0xFFD700 = Gold
    @JsonProperty("6")
    LIGHTNING_DAMAGE(new int[] { 6 }, 0xFFD700),
    // 0xD02090 = Medium Violet Red
    @JsonProperty("7")
    CHAOS_DAMAGE(new int[] { 7 }, 0xD02090),
    // TODO check if "10" really is eternal orb imprint too
    // TODO check what ETERNAL_ORB_IMPRINT's colour should really be (default to BLACK)
    @JsonProperty("8")
    @JsonAlias({"9", "10"})
    ETERNAL_ORB_IMPRINT(new int[] { 8, 9, 10}, 0x000000 );

    private final int[] values;
    private final int colourHex;

    public static ValueType from(int i) {
        for (ValueType type : ValueType.values()) {
            for (int j : type.values) {
                if (i == j) {
                    return type;
                }
            }
        }
        return null;
    }
}
