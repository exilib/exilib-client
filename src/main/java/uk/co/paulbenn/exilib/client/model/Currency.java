package uk.co.paulbenn.exilib.client.model;

import uk.co.paulbenn.exilib.client.util.Enums;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Getter
@RequiredArgsConstructor
public enum Currency {

    ORB_OF_ALTERATION("Orb of Alteration", "alt"),
    ORB_OF_FUSING("Orb of Fusing", "fusing"),
    ORB_OF_ALCHEMY("Orb of Alchemy", "alch"),
    CHAOS_ORB("Chaos Orb", "chaos"),
    GEMCUTTERS_PRISM("Gemcutter's Prism", "gcp"),
    EXALTED_ORB("Exalted Orb", "exalted"),
    CHROMATIC_ORB("Chromatic Orb", "chrome"),
    JEWELLERS_ORB("Jeweller's Orb", "jewellers"),
    ENGINEERS_ORB("Engineer's Orb", "engineers"),
    INFUSED_ENGINEERS_ORB("Infused Engineer's Orb", "infused-engineers-orb"),
    ORB_OF_CHANCE("Orb of Chance", "chance"),
    CARTOGRAPHERS_CHISEL("Cartographer's Chisel", "chisel"),
    ORB_OF_SCOURING("Orb of Scouring", "scour"),
    BLESSED_ORB("Blessed Orb", "blessed"),
    ORB_OF_REGRET("Orb of Regret", "regret"),
    REGAL_ORB("Regal Orb", "regal"),
    DIVINE_ORB("Divine Orb", "divine"),
    VAAL_ORB("Vaal Orb", "vaal"),
    ORB_OF_ANNULMENT("Orb of annulment", "annul"),
    ORB_OF_BINDING("Orb of Binding", "orb-of-binding"),
    ANCIENT_ORB("Ancient Orb", "ancient-orb"),
    ORB_OF_HORIZONS("Orb of Horizons", "orb-of-horizons"),
    HARBINGERS_ORB("Harbinger's Orb", "harbingers-orb"),
    SCROLL_OF_WISDOM("Scroll of Wisdom", "wisdom"),
    PORTAL_SCROLL("Portal Scroll", "portal"),
    ARMOURERS_SCRAP("Armourer's Scrap", "scrap"),
    BLACKSMITHS_WHETSTONE("Blacksmith's Whetstone", "whetstone"),
    GLASSBLOWERS_BAUBLE("Glassblower's Bauble", "bauble"),
    ORB_OF_TRANSMUTATION("Orb of Transmutation", "transmute"),
    ORB_OF_AUGMENTATION("Orb of Augmentation", "aug"),
    MIRROR_OF_KALANDRA("Mirror of Kalandra", "mirror"),
    ETERNAL_ORB("Eternal Orb", "eternal"),
    PERANDUS_COIN("Perandus Coin", "p"),
    ROGUES_MARKER("Rogue's Marker", "rogues-marker"),
    SILVER_COIN("Silver Coin", "silver"),
    CRUSADERS_EXALTED_ORB("Crusader's Exalted Orb", "crusaders-exalted-orb"),
    REDEMEERS_EXALTED_ORB("Redeemer's Exalted Orb", "redeemers-exalted-orb"),
    HUNTERS_EXALTED_ORB("Hunter's Exalted Orb", "hunters-exalted-orb"),
    WARLORDS_EXALTED_ORB("Warlord's Exalted Orb", "warlords-exalted-orb"),
    AWAKENERS_ORB("Awakener's Orb", "awakeners-orb"),
    MAVENS_ORB("Maven's Orb", "mavens-orb"),
    FACETORS_LENS("Facetor's Lens", "facetors"),
    PRIME_REGRADING_LENS("Prime Regrading Lens", "prime-regrading-lens"),
    SECONDARY_REGRADING_LENS("Secondary Regrading Lens", "secondary-regrading-lens"),
    TEMPERING_ORB("Tempering Orb", "tempering-orb"),
    TAILORING_ORB("Tailoring Orb", "tailoring-orb"),
    STACKED_DECK("Stacked Deck", "stacked-deck"),
    RITUAL_VESSEL("Ritual Vessel", "ritual-vessel"),
    SIMPLE_SEXTANT("Simple Sextant", "apprentice-sextant"),
    PRIME_SEXTANT("Prime Sextant", "journeyman-sextant"),
    AWAKENED_SEXTANT("Awakened Sextant", "master-sextant"),
    ELEVATED_SEXTANT("Elevated Sextant", "elevated-sextant"),
    ORB_OF_UNMAKING("Orb of Unmaking", "orb-of-unmaking"),
    BLESSING_OF_XOPH("Blessing of Xoph", "blessing-xoph"),
    BLESSING_OF_TUL("Blessing of Tul", "blessing-tul"),
    BLESSING_OF_ESH("Blessing of Esh", "blessing-esh"),
    BLESSING_OF_UUL_NETOL("Blessing of Uul-Netol", "blessing-uul-netol"),
    BLESSING_OF_CHAYULA("Blessing of Chayula", "blessing-chayula");

    private static final Map<String, Currency> lookUpByName = Enums.keyMap(Currency.class, Currency::getName);
    private static final Map<String, Currency> lookUpById = Enums.keyMap(Currency.class, Currency::getTradeId);

    private static Map<String, Currency> alternateIds = new AlternateIds().getMap();

    private final String name;
    private final String tradeId;

    public static Currency getByName(String name) {
        return lookUpByName.get(name);
    }

    public static Currency getByTradeId(String tradeId) {
        return Optional.ofNullable(lookUpById.get(tradeId)).orElseGet(() -> alternateIds().get(tradeId));
    }

    private static Map<String, Currency> alternateIds() {
        if (alternateIds == null) {
            alternateIds = new HashMap<>();
        }
        return alternateIds;
    }

    @Override
    public String toString() {
        return name;
    }

    @Getter
    private static class AlternateIds {
        private final Map<String, Currency> map;

        AlternateIds() {
            map = new HashMap<>();

            putAll(Set.of("alt", "alts", "alter", "alteration", "alterations"), ORB_OF_ALTERATION);
            putAll(Set.of("fus", "fuse", "fuses", "fusing", "fusings"), ORB_OF_FUSING);
            putAll(Set.of("alc", "alcs", "alch", "alchs", "alchemy", "alchemys"), ORB_OF_ALCHEMY);
            putAll(Set.of("c", "chao", "caos", "chaos", "choas", "chaoss", "chaoses"), CHAOS_ORB);
            putAll(Set.of("gcp", "gcps", "gemc", "prism", "prisms", "gemcutter", "gemcutters"), GEMCUTTERS_PRISM);
            putAll(
                    Set.of("x", "ex", "exs", "exa", "exas", "exalt", "exalts", "exalted", "erhaben", "exalteds"),
                    EXALTED_ORB
            );
            putAll(Set.of("chr", "chrom", "chrome", "chroms", "chromes", "chromatic", "chromatics"), CHROMATIC_ORB);
            putAll(Set.of("jew", "jews", "jewel", "jewels", "jeweller", "jewellers"), JEWELLERS_ORB);
            putAll(Set.of("chance", "chances"), ORB_OF_CHANCE);
            putAll(
                    Set.of("cart", "chis", "chises", "chisel", "chisels", "cartographer", "cartographers"),
                    CARTOGRAPHERS_CHISEL
            );
            putAll(Set.of("scour", "scours", "scouring", "scourings"), ORB_OF_SCOURING);
            putAll(Set.of("bless", "blessed", "blesseds"), BLESSED_ORB);
            putAll(Set.of("regret", "regrets"), ORB_OF_REGRET);
            putAll(Set.of("regal", "regals"), REGAL_ORB);
            putAll(Set.of("div", "divine", "divines"), DIVINE_ORB);
            putAll(Set.of("vaal", "vaals", "corruption"), VAAL_ORB);
            putAll(Set.of("wis", "wises", "wisdom", "wisdoms"), SCROLL_OF_WISDOM);
            putAll(Set.of("port", "ports", "portal", "portals"), PORTAL_SCROLL);
            putAll(Set.of("arm", "scr", "scrap", "scraps", "armourers"), ARMOURERS_SCRAP);
            putAll(Set.of("bla", "whe", "whetstone", "whetstones", "blacksmiths"), BLACKSMITHS_WHETSTONE);
            putAll(Set.of("ba", "gla", "glass", "bauble", "baubles", "glassblowers"), GLASSBLOWERS_BAUBLE);
            putAll(
                    Set.of("tra", "trans", "transmute", "transmutes", "transmutation", "transmutations"),
                    ORB_OF_TRANSMUTATION
            );
            putAll(Set.of("aug", "augs", "augment", "augments", "augmentation", "augmentations"), ORB_OF_AUGMENTATION);
            putAll(Set.of("mir", "kal", "mirr", "mirror", "mirrors", "kalandra", "kalandras"), MIRROR_OF_KALANDRA);
            putAll(Set.of("et", "ete", "eter", "eternal", "eternals"), ETERNAL_ORB);
            putAll(Set.of("pc", "per", "coin", "coins", "shekel", "shekels", "perandus"), PERANDUS_COIN);
            putAll(Set.of("sc", "navali", "silver", "silvers"), SILVER_COIN);
        }

        private void putAll(Set<String> alternateIds, Currency currency) {
            for (String key : alternateIds) {
                map.put(key, currency);
            }
        }
    }
}
