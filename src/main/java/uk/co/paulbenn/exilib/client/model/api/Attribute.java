package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.paulbenn.exilib.client.util.Enums;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public enum Attribute {
    @JsonProperty("G")
    GENERIC("G"),
    @JsonProperty("S")
    STRENGTH("S"),
    @JsonProperty("D")
    DEXTERITY("D"),
    @JsonProperty("I")
    INTELLIGENCE("I"),
    @JsonProperty("A")
    ABYSS("A"),
    @JsonProperty("DV")
    DELVE("DV");

    private static final Map<String, Attribute> lookUp = Enums.keyMap(Attribute.class, Attribute::getKey);

    private final String key;

    public static Attribute from(String attribute) {
        return lookUp.get(attribute.toUpperCase());
    }
}
