package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import uk.co.paulbenn.exilib.client.deserializer.ValueArrayDeserializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Requirement {

    @JsonProperty("displayMode")
    private DisplayMode displayMode;
    @JsonProperty("name")
    private String name;
    @JsonProperty("progress")
    private Double progress;
    @JsonProperty("type")
    private Integer type;
    @JsonProperty("values")
    @JsonDeserialize(using = ValueArrayDeserializer.class)
    private Value[] values;
    @JsonProperty("suffix")
    private String suffix;

    @Override
    public String toString() {
        return String.format("REQUIREMENT '%s', values %s", name, Arrays.toString(values));
    }
}
