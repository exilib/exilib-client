package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.paulbenn.exilib.client.util.Enums;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public enum DisplayMode {

    // e.g. for item PROPERTIES
    @JsonProperty("0")
    NAME_THEN_VALUE(0),
    // e.g. for item REQUIREMENTS
    @JsonProperty("1")
    VALUE_THEN_NAME(1),
    // e.g. for gem experience
    @JsonProperty("2")
    PROGRESS_BAR(2),
    // e.g. for flasks: "Lasts %0 seconds"
    @JsonProperty("3")
    TEMPLATED(3);

    private static final Map<Integer, DisplayMode> lookUp = Enums.keyMap(DisplayMode.class, DisplayMode::getValue);

    private final int value;

    public static DisplayMode from(int displayModeValue) {
        return lookUp.get(displayModeValue);
    }
}
