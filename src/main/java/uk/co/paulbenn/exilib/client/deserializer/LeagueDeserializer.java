package uk.co.paulbenn.exilib.client.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import uk.co.paulbenn.exilib.client.model.api.League;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class LeagueDeserializer extends StdDeserializer<League> {

    public LeagueDeserializer() {
        this(League.class);
    }

    private LeagueDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public League deserialize(JsonParser jp, DeserializationContext ctx) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        if(node.isTextual()) {
            String leagueName = node.asText();
            return new League(leagueName);
        } else {
            log.error("'league' key does not does not map to a String: {}", node);
        }
        return null;
    }
}
