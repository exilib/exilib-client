package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uk.co.paulbenn.exilib.client.util.UnrecognizedFields;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ExtendedInfo {

    @JsonProperty("category")
    private String category;
    @JsonProperty("subcategories")
    private String[] subCategories;
    @JsonProperty("baseType")
    private String baseType;
    @JsonProperty("prefixes")
    private int prefixes;
    @JsonProperty("suffixes")
    private int suffixes;

    private Map<String, Object> unrecognizedFields;

    @JsonAnyGetter
    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields == null ? new HashMap<>() : unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedField(String key, Object value) {
        UnrecognizedFields.report(this.getClass(), key, value);

        if (unrecognizedFields == null) {
            unrecognizedFields = new HashMap<>();
        }

        unrecognizedFields.put(key, value);
    }
}
