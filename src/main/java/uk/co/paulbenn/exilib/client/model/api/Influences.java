package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.paulbenn.exilib.client.util.UnrecognizedFields;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Influences {
    @JsonProperty("shaper")
    private Boolean shaper;
    @JsonProperty("elder")
    private Boolean elder;
    @JsonProperty("crusader")
    private Boolean crusader;
    @JsonProperty("hunter")
    private Boolean hunter;
    @JsonProperty("redeemer")
    private Boolean redeemer;
    @JsonProperty("warlord")
    private Boolean warlord;

    private Map<String, Object> unrecognizedFields;

    @JsonAnyGetter
    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields == null ? new HashMap<>() : unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedField(String key, Object value) {
        UnrecognizedFields.report(this.getClass(), key, value);

        if (unrecognizedFields == null) {
            unrecognizedFields = new HashMap<>();
        }

        unrecognizedFields.put(key, value);
    }
}
