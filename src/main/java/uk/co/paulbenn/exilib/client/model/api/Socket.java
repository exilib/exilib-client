package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Socket {

    @JsonProperty("attr")
    private Attribute attribute;
    @JsonProperty("group")
    private Integer group;
    @JsonProperty("sColour")
    private Colour colour;

    public enum Colour {
        @JsonProperty("W")
        WHITE,
        @JsonProperty("R")
        RED,
        @JsonProperty("G")
        GREEN,
        @JsonProperty("B")
        BLUE,
        @JsonProperty("A")
        ABYSS,
        @JsonProperty("DV")
        DELVE;
    }
}
