package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.paulbenn.exilib.client.util.Enums;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public enum StashType {
    @JsonProperty("NormalStash")
    NORMAL("NormalStash"),
    @JsonProperty("PremiumStash")
    PREMIUM("PremiumStash"),
    @JsonProperty("QuadStash")
    QUAD("QuadStash"),
    @JsonProperty("UniqueStash")
    UNIQUE("UniqueStash"),
    @JsonProperty("CurrencyStash")
    CURRENCY("CurrencyStash"),
    @JsonProperty("MapStash")
    MAP("MapStash"),
    @JsonProperty("FragmentStash")
    FRAGMENT("FragmentStash"),
    @JsonProperty("DivinationCardStash")
    DIVINATION_CARD("DivinationCardStash"),
    @JsonProperty("EssenceStash")
    ESSENCE("EssenceStash"),
    @JsonProperty("DelveStash")
    DELVE("DelveStash"),
    @JsonProperty("DeliriumStash")
    DELIRIUM("DeliriumStash"),
    @JsonProperty("BlightStash")
    BLIGHT("BlightStash"),
    @JsonProperty("MetamorphStash")
    METAMORPH("MetamorphStash");

    private static final Map<String, StashType> lookUp = Enums.keyMap(StashType.class, StashType::getStashTypeName);

    private final String stashTypeName;

    public static StashType from(String stashTypeName) {
        return lookUp.get(stashTypeName);
    }
}
