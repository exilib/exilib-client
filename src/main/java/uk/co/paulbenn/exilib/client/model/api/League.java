package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import uk.co.paulbenn.exilib.client.deserializer.LeagueDeserializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Set;

@JsonDeserialize(using = LeagueDeserializer.class)
@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
public class League {

    public static final League STANDARD_SOFTCORE = new League("Standard");
    public static final League STANDARD_HARDCORE = new League("Hardcore");

    private static final Set<League> STANDARD_LEAGUES = Set.of(
            STANDARD_SOFTCORE,
            STANDARD_HARDCORE
    );

    private final String name;

    public boolean isStandardSoftcore() {
        return this.equals(STANDARD_SOFTCORE);
    }

    public boolean isStandardHardcore() {
        return this.equals(STANDARD_HARDCORE);
    }

    public boolean isPermanent() {
        return isStandardHardcore() || isStandardHardcore();
    }

    public boolean isSoftcore() {
        return !isHardcore();
    }

    public boolean isHardcore() {
        return name.toLowerCase().contains("hardcore");
    }

    public boolean isSoloSelfFound() {
        return name.toLowerCase().contains("ssf");
    }

    public boolean isSpecialEvent() {
        return name.toLowerCase().contains("event");
    }
}
