package uk.co.paulbenn.exilib.client.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class UnrecognizedFields {

    private static final int MAX_LENGTH_BEFORE_TRUNCATE = 256;

    private UnrecognizedFields() {
        throw new UnsupportedOperationException();
    }

    public static void report(Class<?> modelClass, String key, Object value) {
        log.warn(
                "{} > Unrecognized key '{}', maps to '{}'",
                modelClass.getSimpleName(),
                key,
                truncatedToString(value)
        );
    }

    private static String truncatedToString(Object value) {
        String toString = value.toString();

        if (toString.length() <= MAX_LENGTH_BEFORE_TRUNCATE) {
            return toString;
        }

        return toString.substring(0, MAX_LENGTH_BEFORE_TRUNCATE - 3) + "...";
    }
}
