package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uk.co.paulbenn.exilib.client.model.Price;
import uk.co.paulbenn.exilib.client.model.Sockets;
import uk.co.paulbenn.exilib.client.util.UnrecognizedFields;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Item {

    @JsonProperty("abyssJewel")
    private Boolean abyssJewel;
    @JsonProperty("additionalProperties")
    private Property[] additionalProperties;
    @JsonProperty("artFilename")
    private String artFilename;
    @JsonProperty("colour")
    private Attribute gemAttribute;
    @JsonProperty("corrupted")
    private Boolean corrupted;
    @JsonProperty("cosmeticMods")
    private String[] cosmeticMods;
    @JsonProperty("craftedMods")
    private String[] craftedMods;
    @JsonProperty("delve")
    private Boolean delve;
    @JsonProperty("descrText")
    private String descriptionText;
    @JsonProperty("duplicated")
    private Boolean duplicated;
    @JsonProperty("elder")
    private Boolean elder;
    @JsonProperty("enchantMods")
    private String[] enchantMods;
    @JsonProperty("explicitMods")
    private String[] explicitMods;
    @JsonProperty("extended")
    private ExtendedInfo extendedInfo;
    @JsonProperty("flavourText")
    private String[] flavourText;
    @JsonProperty("fractured")
    private Boolean fractured;
    @JsonProperty("fracturedMods")
    private String[] fracturedMods;
    @JsonProperty("frameType")
    private FrameType frameType;
    @JsonProperty("h")
    private Integer height;
    @JsonProperty("hybrid")
    private HybridGemInfo hybridGemInfo;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("id")
    private String itemId;
    @JsonProperty("identified")
    private Boolean identified;
    @JsonProperty("ilvl")
    private Integer itemLevel;
    @JsonProperty("itemLevel")
    private Integer itemLevelNew;
    @JsonProperty("implicitMods")
    private String[] implicitMods;
    @JsonProperty("incubatedItem")
    private IncubatedItem incubatedItem;
    @JsonProperty("influences")
    private Influences influences;
    @JsonProperty("inventoryId")
    private String inventoryId;
    @JsonProperty("isRelic")
    private Boolean isRelic;
    @JsonProperty("league")
    private League league;
    @JsonProperty("lockedToCharacter")
    private Boolean lockedToCharacter;
    @JsonProperty("maxStackSize")
    private Integer maxStackSize;
    @JsonProperty("name")
    private String name;
    @JsonProperty("nextLevelRequirements")
    private Requirement[] nextLevelRequirements;
    @JsonProperty("note")
    private String note;
    @JsonProperty("properties")
    private Property[] properties;
    @JsonProperty("prophecyDiffText")
    private String prophecyDiffText;
    @JsonProperty("prophecyText")
    private String prophecyText;
    @JsonProperty("replica")
    private Boolean replica;
    @JsonProperty("requirements")
    private Requirement[] requirements;
    @JsonProperty("secDescrText")
    private String secondDescriptionText;
    @JsonProperty("shaper")
    private Boolean shaper;
    @JsonProperty("socket")
    private Integer gemPosition;
    @JsonProperty("socketedItems")
    private Item[] socketedItems;
    @JsonProperty("sockets")
    private Socket[] sockets;
    @JsonProperty("stackSize")
    private Integer stackSize;
    @JsonProperty("stackSizeText")
    private String stackSizeText;
    @JsonProperty("support")
    private Boolean support;
    @JsonProperty("synthesised")
    private Boolean synthesised;
    @JsonProperty("talismanTier")
    private Integer talismanTier;
    @JsonProperty("typeLine")
    private String typeLine;
    @JsonProperty("utilityMods")
    private String[] utilityMods;
    @JsonProperty("vaal")
    private Vaal vaal;
    @JsonProperty("veiled")
    private Boolean veiled;
    @JsonProperty("veiledMods")
    private String[] veiledMods;
    @JsonProperty("verified")
    private Boolean verified;
    @JsonProperty("w")
    private Integer width;
    @JsonProperty("x")
    private Integer stashPosX;
    @JsonProperty("y")
    private Integer stashPosY;

    // LAZY
    @Getter(AccessLevel.NONE)
    private Price price;

    // LAZY
    @Getter(AccessLevel.NONE)
    private Sockets socketsParsed;

    private Map<String, Object> unrecognizedFields;

    public Price getPrice() {
        if (price == null) {
            price = Price.parsePrice(note);
        }

        return price;
    }

    public Sockets getSocketsParsed() {
        if (socketsParsed == null) {
            socketsParsed = Sockets.ofSockets(sockets);
        }

        return socketsParsed;
    }

    public boolean isRaceReward() {
        Pattern raceRewardPattern = Pattern.compile("(.*)raceReward(.*)", Pattern.CASE_INSENSITIVE);

        if (unrecognizedFields == null) {
            return false;
        }

        return unrecognizedFields.keySet().stream().anyMatch(s -> raceRewardPattern.matcher(s).matches());
    }

    @JsonAnyGetter
    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields == null ? new HashMap<>() : unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedField(String key, Object value) {
        UnrecognizedFields.report(this.getClass(), key, value);

        if (unrecognizedFields == null) {
            unrecognizedFields = new HashMap<>();
        }

        unrecognizedFields.put(key, value);
    }

    @Override
    public String toString() {
        return String.format("ITEM (%s league, %s %s)", league.getName(), frameType, name);
    }
}
