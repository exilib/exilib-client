package uk.co.paulbenn.exilib.client.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import uk.co.paulbenn.exilib.client.model.api.Value;
import uk.co.paulbenn.exilib.client.model.api.ValueType;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ValueArrayDeserializer extends StdDeserializer<Value[]> {

    public ValueArrayDeserializer() {
        this(Value[].class);
    }

    private ValueArrayDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Value[] deserialize(JsonParser jp, DeserializationContext ctx) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        if(node.isArray()) {
            List<JsonNode> valueNodes = new ArrayList<>();
            node.elements().forEachRemaining(valueNodes::add);
            Value[] values = new Value[node.size()];
            for(int i = 0; i < valueNodes.size(); i++) {
                JsonNode valueArrElem = valueNodes.get(i);
                if(valueArrElem.isArray()) {
                    values[i] = getAsValue(valueArrElem);
                } else {
                    log.error("'values' element does not map to a JsonArray: {}", valueArrElem);
                }
            }
            return values;
        } else {
            log.error("'values' does not map to a JsonArray: {}", node);
        }
        return new Value[0];
    }

    private Value getAsValue(JsonNode valueArr) {
        Value value = new Value();
        List<JsonNode> list = new ArrayList<>();
        valueArr.elements().forEachRemaining(list::add);

        if(list.size() < 2) {
            log.error("'values' element contains less than two fields: {}", valueArr);
        } else if(list.size() > 2) {
            log.error("'values' element contains more than two fields: {}", valueArr);
        } else {
            JsonNode first = valueArr.get(0);
            if(first.isTextual()) {
                value.setValue(first.asText());
            } else {
                log.error("'values' element key does not map to a String: {}", first);
            }

            JsonNode second = valueArr.get(1);
            if(second.isInt()) {
                value.setValueType(ValueType.from(second.asInt()));
            } else {
                log.error("'values' element value does not map to an int: {}", second);
            }
        }

        return value;
    }
}
