package uk.co.paulbenn.exilib.client.util;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class Enums {

    private Enums() {
        throw new UnsupportedOperationException();
    }

    public static <K, V extends Enum<V>> Map<K, V> keyMap(Class<V> enumClass, Function<V, K> keyExtractor) {
        return Arrays.stream(enumClass.getEnumConstants())
                .collect(Collectors.toMap(keyExtractor, Function.identity()));
    }
}
