package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uk.co.paulbenn.exilib.client.util.UnrecognizedFields;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Stash {

    @JsonProperty("id")
    private String stashId;
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("lastCharacterName")
    private String lastCharacterName;
    @JsonProperty("stash")
    private String stashName;
    @JsonProperty("stashType")
    private StashType stashType;
    @JsonProperty("league")
    private League league;
    @JsonProperty("items")
    private List<Item> items;
    @JsonProperty("public")
    private Boolean isPublic;

    private Map<String, Object> unrecognizedFields;

    @JsonAnyGetter
    public Map<String, Object> getUnrecognizedFields() {
        return unrecognizedFields == null ? new HashMap<>() : unrecognizedFields;
    }

    @JsonAnySetter
    public void setUnrecognizedField(String key, Object value) {
        UnrecognizedFields.report(this.getClass(), key, value);

        if (unrecognizedFields == null) {
            unrecognizedFields = new HashMap<>();
        }

        unrecognizedFields.put(key, value);
    }

    @Override
    public String toString() {
        return String.format(
                "STASH TAB (%s league, %s, type %s, %d items",
                league.getName(),
                (isPublic ? "public" : "private"),
                stashType,
                items.size()
        );
    }
}
