package uk.co.paulbenn.exilib.client.model;

import uk.co.paulbenn.exilib.client.util.Enums;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.EnumSet;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum Fragment {

    SACRIFICE_AT_DUSK("Sacrifice at Dusk"),
    SACRIFICE_AT_MIDNIGHT("Sacrifice at Midnight"),
    SACRIFICE_AT_DAWN("Sacrifice at Dawn"),
    SACRIFICE_AT_NOON("Sacrifice at Noon"),

    MORTAL_GRIEF("Mortal Grief"),
    MORTAL_RAGE("Mortal Rage"),
    MORTAL_HOPE("Mortal Hope"),
    MORTAL_IGNORANCE("Mortal Ignorance"),

    VOLKUURS_KEY("Volkuur's Key"),
    EBERS_KEY("Eber's Key"),
    YRIELS_KEY("Yriel's Key"),
    INYAS_KEY("Inya's Key"),

    FRAGMENT_OF_THE_HYDRA("Fragment of the Hydra"),
    FRAGMENT_OF_THE_PHOENIX("Fragment of the Phoenix"),
    FRAGMENT_OF_THE_MINOTAUR("Fragment of the Minotaur"),
    FRAGMENT_OF_THE_CHIMERA("Fragment of the Chimera"),

    FRAGMENT_OF_PURIFICATION("Fragment of Purification"),
    FRAGMENT_OF_CONSTRICTION("Fragment of Constriction"),
    FRAGMENT_OF_ERADICATION("Fragment of Eradication"),
    FRAGMENT_OF_ENSLAVEMENT("Fragment of Enslavement"),

    FRAGMENT_OF_KNOWLEDGE("Fragment of Knowledge"),
    FRAGMENT_OF_SHAPE("Fragment of Shape"),
    FRAGMENT_OF_EMPTINESS("Fragment of Emptiness"),
    FRAGMENT_OF_TERROR("Fragment of Terror");

    private static final Map<String, Fragment> lookUpByName = Enums.keyMap(Fragment.class, Fragment::getName);

    private final String name;

    public static Fragment getByName(String name) {
        return lookUpByName.get(name);
    }

    public Fragment.Set getSet() {
        return Set.forFragment(this);
    }

    @Getter
    @RequiredArgsConstructor
    public enum Set {
        APEX_OF_SACRIFICE(
                "The Apex of Sacrifice",
                EnumSet.of(SACRIFICE_AT_DUSK, SACRIFICE_AT_MIDNIGHT, SACRIFICE_AT_DAWN, SACRIFICE_AT_NOON)
        ),
        ALLURING_ABYSS(
                "The Alluring Abyss",
                EnumSet.of(MORTAL_GRIEF, MORTAL_RAGE, MORTAL_HOPE, MORTAL_IGNORANCE)
        ),
        PALE_COURT(
                "The Pale Court",
                EnumSet.of(VOLKUURS_KEY, EBERS_KEY, YRIELS_KEY, INYAS_KEY)
        ),
        SHAPERS_REALM(
                "The Shaper's Realm",
                EnumSet.of(
                        FRAGMENT_OF_THE_HYDRA,
                        FRAGMENT_OF_THE_PHOENIX,
                        FRAGMENT_OF_THE_MINOTAUR,
                        FRAGMENT_OF_THE_CHIMERA
                )
        ),
        ABSENCE_OF_VALUE_AND_MEANING(
                "Absence of Value and Meaning",
                EnumSet.of(
                        FRAGMENT_OF_PURIFICATION,
                        FRAGMENT_OF_CONSTRICTION,
                        FRAGMENT_OF_ERADICATION,
                        FRAGMENT_OF_ENSLAVEMENT
                )
        ),
        ABSENCE_OF_VALUE_AND_MEANING_SHAPERS_REALM(
                "Absence of Value and Meaning (Shaper's Realm)",
                EnumSet.of(FRAGMENT_OF_KNOWLEDGE, FRAGMENT_OF_SHAPE, FRAGMENT_OF_EMPTINESS, FRAGMENT_OF_TERROR)
        );

        private static final Map<String, Fragment.Set> lookUpByName = Enums.keyMap(
                Fragment.Set.class,
                Fragment.Set::getName
        );

        private final String name;
        private final java.util.Set<Fragment> fragments;

        public static Fragment.Set getByName(String name) {
            return lookUpByName.get(name);
        }

        public static Fragment.Set forFragment(Fragment fragment) {
            for (Set set : Set.values()) {
                if (set.fragments.contains(fragment)) {
                    return set;
                }
            }

            return null;
        }

        @Override
        public String toString() {
            return name + String.format(" | %d fragments)", fragments.size());
        }
    }
}
