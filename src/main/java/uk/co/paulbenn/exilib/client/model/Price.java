package uk.co.paulbenn.exilib.client.model;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import uk.co.paulbenn.exilib.client.util.Enums;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class Price {

    private static final Pattern PRICE_PATTERN = Pattern.compile(
            "\\s*(~(?:price|b/o|c/o))\\s+((?:\\.?\\d+)|(?:\\d+(?:[.,]\\d+)?)|(?:\\d+/\\d+))\\s+([a-zA-Z]+)\\s*"
    );

    private final Type type;
    private final BigDecimal amount;
    private final Currency currency;

    public static Pattern getPattern() {
        return PRICE_PATTERN;
    }

    public static boolean isPrice(String note) {
        return PRICE_PATTERN.matcher(note).matches();
    }

    public static Price parsePrice(String price) {
        if (price == null) {
            return null;
        }

        Matcher matcher = PRICE_PATTERN.matcher(price);

        if (!matcher.matches()) {
            return null;
        }

        // group 0 = entire match
        Type type = Type.from(matcher.group(1));
        BigDecimal amount = parseBigDecimal(matcher.group(2));
        Currency currency = Currency.getByTradeId(matcher.group(3));

        return new Price(type, amount, currency);
    }

    private static BigDecimal parseBigDecimal(String amount) {
        if (amount.contains("/")) {
            String[] fractionalParts = amount.split("/");

            String numerator = fractionalParts[0];
            String denominator = fractionalParts[1];

            return new BigDecimal(numerator).divide(new BigDecimal(denominator), 4, RoundingMode.HALF_DOWN);
        }

        return new BigDecimal(amount);
    }

    @Override
    public String toString() {
        return type.getText() + " " + amount + " " + currency.getTradeId();
    }

    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PACKAGE)
    public enum Type {

        PRICE("~price"),
        BUY_OUT("~b/o"),
        CURRENT_OFFER("~c/o");

        private static final Map<String, Type> lookUp = Enums.keyMap(Type.class, Type::getText);

        private final String text;

        public static Type from(String priceType) {
            return lookUp.get(priceType);
        }

        @Override
        public String toString() {
            return text;
        }
    }
}
