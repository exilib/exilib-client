package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import uk.co.paulbenn.exilib.client.util.SharedObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DisplayModeTest {

    @ParameterizedTest
    @MethodSource("deserializeDisplayModeArgs")
    void deserializeDisplayMode(String json, DisplayMode expected) {
        DisplayMode displayMode = SharedObjectMapper.readValue(json, DisplayMode.class);
        assertThat(displayMode).isEqualTo(expected);
    }

    @Test
    void deserializeDisplayModeNonExistent() {
        assertThrows(InvalidFormatException.class, () -> SharedObjectMapper.readValue("5", DisplayMode.class));
    }

    private static Stream<Arguments> deserializeDisplayModeArgs() {
        return Stream.of(
                Arguments.of("0", DisplayMode.NAME_THEN_VALUE),
                Arguments.of("1", DisplayMode.VALUE_THEN_NAME),
                Arguments.of("2", DisplayMode.PROGRESS_BAR),
                Arguments.of("3", DisplayMode.TEMPLATED)
        );
    }
}
