package uk.co.paulbenn.exilib.client.model.api;

import uk.co.paulbenn.exilib.client.util.SharedObjectMapper;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

class StashTest {

    @Test
    void deserializeStash() {
        Stash stash = SharedObjectMapper.readValue(Paths.get("src/test/resources/stash.json"), Stash.class);

        assertThat(stash.getAccountName()).isEqualTo("ACCOUNT-NAME");
        assertThat(stash.getLastCharacterName()).isEqualTo("LAST-CHARACTER-NAME");
        assertThat(stash.getStashId()).isEqualTo("0000000000000000000000000000000000000000000000000000000000000000");
        assertThat(stash.getStashName()).isEqualTo("STASH-TAB-NAME");
        assertThat(stash.getStashType()).isEqualTo(StashType.NORMAL);
        assertThat(stash.getItems()).isEmpty();
        assertThat(stash.getIsPublic()).isFalse();
    }
}
