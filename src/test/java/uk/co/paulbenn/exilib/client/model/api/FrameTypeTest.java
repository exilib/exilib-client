package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import uk.co.paulbenn.exilib.client.util.SharedObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FrameTypeTest {

    @ParameterizedTest
    @MethodSource("deserializeFrameTypeArgs")
    void deserializeFrameType(String json, FrameType expected) {
        FrameType frameType = SharedObjectMapper.readValue(json, FrameType.class);
        assertThat(frameType).isEqualTo(expected);
    }

    @Test
    void deserializeFrameTypeNonExistent() {
        assertThrows(InvalidFormatException.class, () -> SharedObjectMapper.readValue("10", FrameType.class));
    }

    private static Stream<Arguments> deserializeFrameTypeArgs() {
        return Stream.of(
                Arguments.of("0", FrameType.NORMAL),
                Arguments.of("1", FrameType.MAGIC),
                Arguments.of("2", FrameType.RARE),
                Arguments.of("3", FrameType.UNIQUE),
                Arguments.of("4", FrameType.GEM),
                Arguments.of("5", FrameType.CURRENCY),
                Arguments.of("6", FrameType.DIVINATION_CARD),
                Arguments.of("7", FrameType.QUEST_ITEM),
                Arguments.of("8", FrameType.PROPHECY),
                Arguments.of("9", FrameType.RELIC)
        );
    }
}
