package uk.co.paulbenn.exilib.client.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.nio.file.Path;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SharedObjectMapper {

    private static ObjectMapper instance;

    public static ObjectMapper instance() {
        if (instance == null) {
            instance = new ObjectMapper();
            instance.findAndRegisterModules();
        }

        return instance;
    }

    @SneakyThrows
    public static <V> V readValue(String string, Class<V> valueType) {
        return instance().readValue(string, valueType);
    }

    @SneakyThrows
    public static <V> V readValue(Path path, Class<V> valueType) {
        return instance().readValue(path.toFile(), valueType);
    }
}
