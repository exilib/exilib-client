package uk.co.paulbenn.exilib.client.model.api;

import uk.co.paulbenn.exilib.client.util.SharedObjectMapper;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

class ChunkTest {

    @Test
    void deserializeChunk() {
        Chunk chunk = SharedObjectMapper.readValue(Paths.get("src/test/resources/chunk.json"), Chunk.class);

        assertThat(chunk.getNextChangeId()).isEqualTo("0000-0000-0000-0000-0000");
        assertThat(chunk.getStashes()).isEmpty();
    }
}
