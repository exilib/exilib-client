package uk.co.paulbenn.exilib.client.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class CurrencyTest {

    @Test
    void allCurrencyIdsAreUnique() {
        List<String> nonUnique = Arrays.stream(Currency.values())
                .map(Currency::getTradeId)
                .collect(Collectors.toList());
        Set<String> unique = Arrays.stream(Currency.values())
                .map(Currency::getTradeId)
                .collect(Collectors.toSet());

        assertThat(nonUnique.size()).isEqualTo(unique.size());
    }

    @Test
    void getByName() {
        assertThat(Currency.getByName(Currency.CHAOS_ORB.getName())).isEqualTo(Currency.CHAOS_ORB);
    }

    @Test
    void nonExistentName() {
        assertThat(Currency.getByName("anything")).isNull();
    }

    @Test
    void getById() {
        assertThat(Currency.getByTradeId(Currency.CHAOS_ORB.getTradeId())).isEqualTo(Currency.CHAOS_ORB);
    }

    @Test
    void nonExistentId() {
        assertThat(Currency.getByTradeId("anything")).isNull();
    }

    @Test
    void getByIdAlternate() {
        assertThat(Currency.getByTradeId("c")).isEqualTo(Currency.CHAOS_ORB);
    }
}
