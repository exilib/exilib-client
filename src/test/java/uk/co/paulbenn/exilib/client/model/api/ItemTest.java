package uk.co.paulbenn.exilib.client.model.api;

import uk.co.paulbenn.exilib.client.model.Currency;
import uk.co.paulbenn.exilib.client.model.Price;
import uk.co.paulbenn.exilib.client.util.SharedObjectMapper;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

class ItemTest {

    @Test
    void deserializeItem() {
        Item item = SharedObjectMapper.readValue(Paths.get("src/test/resources/item.json"), Item.class);

        assertThat(item.getAbyssJewel()).isFalse();
        assertThat(item.getAdditionalProperties()).isEmpty();
        assertThat(item.getArtFilename()).isEqualTo("ART-FILENAME");
        assertThat(item.getGemAttribute()).isEqualTo(Attribute.GENERIC);
        assertThat(item.getCorrupted()).isFalse();
        assertThat(item.getCosmeticMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("COSMETIC-MOD"));
        assertThat(item.getCraftedMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("CRAFTED-MOD"));
        assertThat(item.getDelve()).isFalse();
        assertThat(item.getDescriptionText()).isEqualTo("DESCRIPTION-TEXT");
        assertThat(item.getDuplicated()).isFalse();
        assertThat(item.getElder()).isFalse();
        assertThat(item.getEnchantMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("ENCHANT-MOD"));
        assertThat(item.getExplicitMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("EXPLICIT-MOD"));
        assertThat(item.getExtendedInfo().getCategory()).isEqualTo("CATEGORY-NAME");
        assertThat(item.getExtendedInfo().getSubCategories())
                .hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("SUB-CATEGORY-NAME"));
        assertThat(item.getExtendedInfo().getPrefixes()).isZero();
        assertThat(item.getExtendedInfo().getSuffixes()).isZero();
        assertThat(item.getFlavourText()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("FLAVOUR-TEXT"));
        assertThat(item.getFractured()).isFalse();
        assertThat(item.getFracturedMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("FRACTURED-MOD"));
        assertThat(item.getFrameType()).isEqualTo(FrameType.NORMAL);
        assertThat(item.getHeight()).isZero();
        assertThat(item.getHybridGemInfo().getIsVaalGem()).isFalse();
        assertThat(item.getHybridGemInfo().getBaseTypeName()).isEqualTo("BASE-TYPE-NAME");
        assertThat(item.getHybridGemInfo().getProperties()).isEmpty();
        assertThat(item.getHybridGemInfo().getExplicitMods()).isEmpty();
        assertThat(item.getHybridGemInfo().getSecDescrText()).isEqualTo("SECOND-DESCRIPTION-TEXT");
        assertThat(item.getHybridGemInfo().getIsVaalGem()).isFalse();
        assertThat(item.getHybridGemInfo().getIsVaalGem()).isFalse();
        assertThat(item.getHybridGemInfo().getIsVaalGem()).isFalse();
        assertThat(item.getIcon()).isEqualTo("http://example.org");
        assertThat(item.getItemId()).isEqualTo("0000000000000000000000000000000000000000000000000000000000000000");
        assertThat(item.getItemLevelNew()).isEqualTo(0);
        assertThat(item.getIdentified()).isFalse();
        assertThat(item.getItemLevel()).isZero();
        assertThat(item.getImplicitMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("IMPLICIT-MOD"));
        assertThat(item.getInfluences().getShaper()).isFalse();
        assertThat(item.getInfluences().getElder()).isFalse();
        assertThat(item.getInfluences().getCrusader()).isFalse();
        assertThat(item.getInfluences().getHunter()).isFalse();
        assertThat(item.getInfluences().getRedeemer()).isFalse();
        assertThat(item.getInfluences().getWarlord()).isFalse();
        assertThat(item.getInventoryId()).isEqualTo("STASH-TAB-NAME");
        assertThat(item.getIsRelic()).isFalse();
        assertThat(item.getLeague().getName()).isEqualTo("Standard");
        assertThat(item.getLockedToCharacter()).isFalse();
        assertThat(item.getMaxStackSize()).isZero();
        assertThat(item.getName()).isEqualTo("ITEM-NAME");
        assertThat(item.getNextLevelRequirements()).isEmpty();
        assertThat(item.getNote()).isEqualTo("NOTE");
        assertThat(item.getProperties()).isEmpty();
        assertThat(item.getProphecyDiffText()).isEqualTo("PROPHECY-DIFF-TEXT");
        assertThat(item.getProphecyText()).isEqualTo("PROPHECY-TEXT");
        assertThat(item.getReplica()).isFalse();
        assertThat(item.getRequirements()).isEmpty();
        assertThat(item.getSecondDescriptionText()).isEqualTo("SECOND-DESCRIPTION-TEXT");
        assertThat(item.getShaper()).isFalse();
        assertThat(item.getGemPosition()).isZero();
        assertThat(item.getSocketedItems()).isEmpty();
        assertThat(item.getSockets()).isEmpty();
        assertThat(item.getStackSize()).isZero();
        assertThat(item.getSupport()).isFalse();
        assertThat(item.getTalismanTier()).isZero();
        assertThat(item.getTypeLine()).isEqualTo("TYPE-LINE");
        assertThat(item.getUtilityMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("UTILITY-MOD"));
        assertThat(item.getVaal()).isNull();
        assertThat(item.getVeiled()).isFalse();
        assertThat(item.getVeiledMods()).hasOnlyOneElementSatisfying(s -> assertThat(s).isEqualTo("VEILED-MOD"));
        assertThat(item.getVerified()).isFalse();
        assertThat(item.getWidth()).isZero();
        assertThat(item.getStashPosX()).isZero();
        assertThat(item.getStashPosY()).isZero();
    }

    @Test
    void getPrice() {
        Item item = new Item();
        item.setNote("~price 1 chaos");
        Price price = item.getPrice();

        assertThat(price.getType()).isEqualTo(Price.Type.PRICE);
        assertThat(price.getAmount()).isEqualTo(BigDecimal.ONE);
        assertThat(price.getCurrency()).isEqualTo(Currency.CHAOS_ORB);
    }

    @Test
    void getPriceNoPriceInNote() {
        Item item = new Item();
        item.setNote("test note, no price");

        Price price = item.getPrice();

        assertThat(price).isNull();
    }

    @Test
    void getPriceNoNote() {
        Price price = new Item().getPrice();

        assertThat(price).isNull();
    }
}
