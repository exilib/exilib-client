package uk.co.paulbenn.exilib.client.model;

import org.junit.jupiter.api.Test;
import uk.co.paulbenn.exilib.client.model.api.Attribute;
import uk.co.paulbenn.exilib.client.model.api.Socket;

import static org.assertj.core.api.Assertions.assertThat;

class SocketsTest {

    @Test
    void ofSockets() {
        // R-R B-B-G B

        Socket redSocket = new Socket();
        redSocket.setAttribute(Attribute.STRENGTH);
        redSocket.setGroup(0);
        redSocket.setColour(Socket.Colour.RED);

        Socket blueSocket = new Socket();
        blueSocket.setAttribute(Attribute.INTELLIGENCE);
        blueSocket.setGroup(1);
        blueSocket.setColour(Socket.Colour.BLUE);

        Socket greenSocket = new Socket();
        greenSocket.setAttribute(Attribute.DEXTERITY);
        greenSocket.setGroup(1);
        greenSocket.setColour(Socket.Colour.GREEN);

        Socket blueSocketUnlinked = new Socket();
        blueSocketUnlinked.setAttribute(Attribute.INTELLIGENCE);
        blueSocketUnlinked.setGroup(2);
        blueSocketUnlinked.setColour(Socket.Colour.BLUE);

        Socket[] rawSockets = new Socket[] {
            redSocket,
            redSocket,
            blueSocket,
            blueSocket,
            greenSocket,
            blueSocketUnlinked
        };

        Sockets socketsParsed = Sockets.ofSockets(rawSockets);

        assertThat(socketsParsed.getRedSocketCount()).isEqualTo(2);
        assertThat(socketsParsed.getBlueSocketCount()).isEqualTo(3);
        assertThat(socketsParsed.getGreenSocketCount()).isEqualTo(1);
        assertThat(socketsParsed.getLinkedGroupsCount()).isEqualTo(3);
        assertThat(socketsParsed.getLargestLinkedGroupSize()).isEqualTo(3);
    }
}
