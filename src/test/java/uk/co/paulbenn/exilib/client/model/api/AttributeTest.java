package uk.co.paulbenn.exilib.client.model.api;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import uk.co.paulbenn.exilib.client.util.SharedObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AttributeTest {

    @ParameterizedTest
    @MethodSource("deserializeAttributeArgs")
    void deserializeAttribute(String json, Attribute expected) {
        Attribute attribute = SharedObjectMapper.readValue(json, Attribute.class);
        assertThat(attribute).isEqualTo(expected);
    }

    @Test
    void deserializeDisplayModeNonExistent() {
        assertThrows(InvalidFormatException.class, () -> SharedObjectMapper.readValue("\"Z\"", Attribute.class));
    }

    private static Stream<Arguments> deserializeAttributeArgs() {
        return Stream.of(
                Arguments.of("\"A\"", Attribute.ABYSS),
                Arguments.of("\"G\"", Attribute.GENERIC),
                Arguments.of("\"S\"", Attribute.STRENGTH),
                Arguments.of("\"D\"", Attribute.DEXTERITY),
                Arguments.of("\"I\"", Attribute.INTELLIGENCE),
                Arguments.of("\"DV\"", Attribute.DELVE)
        );
    }
}
