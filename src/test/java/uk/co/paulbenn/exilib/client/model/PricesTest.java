package uk.co.paulbenn.exilib.client.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PricesTest {

    @ParameterizedTest
    @EnumSource(Price.Type.class)
    void isPrice(Price.Type type) {
        assertTrue(Price.isPrice(type + " 1 chaos"));
    }

    @Test
    void isPriceBuyOut() {
        assertTrue(Price.isPrice("~b/o 1 chaos"));
    }

    @Test
    void isPriceCurrentOffer() {
        assertTrue(Price.isPrice("~c/o 1 chaos"));
    }

    @Test
    void isPriceExtraSpaces() {
        assertTrue(Price.isPrice("  ~price  1  chaos  "));
    }

    @Test
    void isPriceDecimal() {
        assertTrue(Price.isPrice("~price 0.1234 exalted"));
    }

    @Test
    void isPriceDecimalNoZero() {
        assertTrue(Price.isPrice("~price .1234 exalted"));
    }

    @Test
    void isPriceFraction() {
        assertTrue(Price.isPrice("~price 1/234 exalted"));
    }

    @Test
    void isPriceNoTilde() {
        assertFalse(Price.isPrice("price 1 chaos"));
    }

    @Test
    void isPriceInvalidType() {
        assertFalse(Price.isPrice("~invalid 1 chaos"));
    }

    @Test
    void isPriceNegativeNumber() {
        assertFalse(Price.isPrice("~price -1 chaos"));
    }

    @Test
    void isPriceDoubleDecimalPoint() {
        assertFalse(Price.isPrice("~price 1.1.1 chaos"));
    }

    @Test
    void parsePrice() {
        Price price = Price.parsePrice("~price 1 chaos");

        assertThat(price.getType()).isEqualTo(Price.Type.PRICE);
        assertThat(price.getAmount()).isEqualTo(BigDecimal.ONE);
        assertThat(price.getCurrency()).isEqualTo(Currency.CHAOS_ORB);
    }

    @Test
    void parsePriceFraction() {
        Price price = Price.parsePrice("~price 1/2 chaos");

        assertThat(price.getType()).isEqualTo(Price.Type.PRICE);
        assertThat(price.getAmount()).isEqualByComparingTo(new BigDecimal("0.5"));
        assertThat(price.getCurrency()).isEqualTo(Currency.CHAOS_ORB);
    }

    @Test
    void toStringTest() {
        Price price = Price.parsePrice("~price 1 chaos");

        assertThat(price.toString()).isEqualTo("~price 1 chaos");
    }
}
